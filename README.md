## FIRMWARE DUMP
### sys_tssi_64_armv82_infinix-user 14 UP1A.231005.007 768486 release-keys
- Transsion Name: Infinix NOTE 30
- TranOS Build: X6833B-H894DEFGHI-U-GL-241210V898
- TranOS Version: xos14.0.0
- Brand: INFINIX
- Model: Infinix-X6833B
- Platform: mt6789 (Helio G99)
- Android Build: UP1A.231005.007
- Android Version: 14
- Kernel Version: 5.10.209
- Security Patch: 2024-12-05
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Screen Density: 480
- Fingerprint: Infinix/X6833B-GL/Infinix-X6833B:14/UP1A.231005.007/241210V898:user/release-keys
